# https://github.com/docker-library/docker
# https://hub.docker.com/_/docker
FROM docker:19.03
RUN apk add --no-cache git \
  && apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
